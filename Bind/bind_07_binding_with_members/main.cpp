#include <map>
#include <string>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/assign.hpp>

using namespace std;
using namespace boost::assign;

void print(const std::string& s)
{
	std::cout << s << "\n";
}

int main()
{
	map<int, string> numbers;
	insert(numbers)(1, "one")(2, "two")(3, "three")(4, "four")(5, "five")(6, "six")(7, "seven");

	for_each(numbers.begin(),
			 numbers.end(),
			 boost::bind(
					 &print,
					 boost::bind(&map<int, string>::value_type::second, _1))
	);
}



