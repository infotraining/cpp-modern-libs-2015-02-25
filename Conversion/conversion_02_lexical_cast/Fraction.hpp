#ifndef FRACTION_HPP
#define FRACTION_HPP

// do��cza standardowe pliki nag��wkowe
#include <iostream>

class Fraction {

  private:
    int numer_;
    int denom_;

  public:
    /* nowo��: klasa wyj�tku
     */
    class DenomIsZero {
    };

    /* domy�lny konstruktor i konstruktor o jednym i dw�ch parametrach
     */
    Fraction(int = 0, int = 1);

    /* mno�enie
     * - nowo��: globalna funkcja zaprzyja�niona, umo�liwia
     * automatyczn� konwersj� typu dla pierwszego argumentu
     */
    friend Fraction operator * (const Fraction&, const Fraction&);

    // przypisanie z mno�eniem
    const Fraction& operator *= (const Fraction&);

    /* por�wnanie
     * - nowo��: globalna funkcja zaprzyja�niona, umo�liwia
     * automatyczn� konwersj� typu dla pierwszego argumentu
     */
    friend bool operator < (const Fraction&, const Fraction&);

    // zapis i odczyt ze strumienia
    void printOn(std::ostream&) const;
    void scanFrom(std::istream&);

    // jawna konwersja typu do double
    double toDouble() const;

    int denom() const
    {
        return denom_;
    }

    int numer() const
    {
        return numer_;
    }
};

/* operator *
 * - globalna funkcja zaprzyja�niona
 * - zdefiniowany jako funkcja rozwijana w miejscu wywo�ania
 */
inline Fraction operator * (const Fraction& a, const Fraction& b)
{
    /* mno�y liczniki i mianowniki
     * - nadal nie skraca u�amka
     */
    return Fraction(a.numer_ * b.numer_, a.denom_ * b.denom_);
}

/* standardowy operator wyj�cia
 * - globalnie przeci��ony i zdefiniowany jako funkcja rozwijana w miejscu
 */
inline
std::ostream& operator << (std::ostream& strm, const Fraction& f)
{
    f.printOn(strm);    // wywo�uje funkcj� sk�adow�
    return strm;        // zwraca strumie� umo�liwiaj�c tworzenie �a�cuch�w wywo�a�
}

/* standardowy operator wej�cia
 * - globalnie przeci��ony i zdefiniowany jako funkcja rozwijana w miejscu
 */
inline
std::istream& operator >> (std::istream& strm, Fraction& f)
{
    f.scanFrom(strm);   // wywo�uje funkcj� sk�adow�
    return strm;        // zwraca strumie� umo�liwiaj�c tworzenie �a�cuch�w wywo�a�
}

#endif  // FRACTION_HPP

