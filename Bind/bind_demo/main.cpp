#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <iterator>
#include <map>
#include <boost/bind.hpp>

using namespace std;

void foo(int x, double y)
{
    cout << "foo(" << x << ", " << y << ")" << endl;
}

void do_stuff(double dx, const string& prefix, int& counter)
{
    ++counter;

    cout << "do_stuff " << prefix << " - dx: " << dx << endl;
}

struct Add
{
    typedef int result_type;

    int operator()(int a, int b) const
    {
        return a + b;
    }

    double operator()(double a, double b) const
    {
        return a + b;
    }
};

class BoundFoo
{
    int arg1;
    double arg2;
    void (*func)(int, double);
public:
    BoundFoo() : func(&foo)
    {}

    void operator()()
    {
        func(arg1, arg2);
    }
};

class Person
{
    string fname_;
    string lname_;
    int age_;
public:
    Person(string fn, string ln, int age) : fname_{fn}, lname_{ln}, age_{age}
    {}

    string last_name() const
    {
        return lname_;
    }

    int age() const
    {
        return age_;
    }

    void print(const string& prefix) const
    {
        cout << prefix << " " << lname_ << " " << fname_ << " - age: " << age_ << endl;
    }
};


int main()
{
    function<void()> f0 = boost::bind(&foo, 1, 3.14);

    f0();
    f0();

    function<void(int)> f1 = boost::bind(&foo, _1, 3.14);

    f1(3); // foo(3, 3.14)
    f1(5);

    function<void(double)> f2 = boost::bind(&foo, 5, _1);

    f2(5.77);

    auto foo_reversed = std::bind(&foo, placeholders::_2, placeholders::_1);

    foo_reversed(4.65, 9);

    string prefix = "text";
    int counter = 0;

    auto f3 = boost::bind(&do_stuff, 3.14, cref(prefix), ref(counter));

    f3();

    cout << "counter: " << counter << endl;

    auto add_5 = std::bind(Add(), placeholders::_1, 5);

    cout << "5 + 10 = " << add_5(10) << endl;

    Person p1 { "Jan", "Kowalski", 54 };

    auto printer1 = boost::bind(&Person::print, &p1, "Osoba:");

    printer1();

    auto printer2 = boost::bind(&Person::print, _1, "Osoba");

    printer2(p1);

    vector<Person> people = { p1, Person{"Ewa", "Nowak", 44 }, Person{"Adam", "Anonim", 73} };

    cout << "\nOsoby:\n";
    for_each(people.begin(), people.end(), boost::bind(&Person::print, _1, "Osoba"));

    cout << "\nNazwiska:\n";
    transform(people.begin(), people.end(), ostream_iterator<string>(cout, "\n"),
              boost::bind(&Person::last_name, _1));

    using Dictionary = map<string, string>;

    Dictionary dict = { {"one", "jeden"}, {"two", "dwa"}, {"three", "trzy"} };

    vector<string> keys;

    transform(dict.begin(), dict.end(), back_inserter(keys),
              boost::bind(&Dictionary::value_type::first, _1));

    cout << "\nKeys:";

    for(const auto& k : keys)
        cout << k << " ";
    cout << endl;

    greater<int> gt;

//    auto retired = find_if(people.begin(), people.end(),
//                           bind(greater<int>(),
//                                    bind(&Person::age, placeholders::_1),
//                                    67));

    auto retired = find_if(people.begin(), people.end(),
                           boost::bind(&Person::age, _1) > 67);

//    auto retied = find_if(people.begin(), people.end(),
//                          [](const Person& p) {  return p.age() > 67; });

    if (retired != people.end())
        retired->print("Retired:");
}

