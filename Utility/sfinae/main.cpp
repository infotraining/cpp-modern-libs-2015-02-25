#include <iostream>
#include <type_traits>
#include <boost/utility/enable_if.hpp>

using namespace std;

//template <bool bool_expression, typename T = void>
//struct EnableIf
//{
//};

//template <typename T>
//struct EnableIf<true, T>
//{
//    using type = T;
//};

template <typename T>
auto f(T a, T b) -> decltype(a * b)
{
    return a * b;
}

template <typename T>
auto foo(T arg) -> typename enable_if<is_integral<T>::value, decltype(arg * arg)>::type
{
    cout << "foo(integral: " << arg << ")" << endl;

    return arg * arg;
}

template <typename T>
auto foo(T arg) -> typename boost::enable_if<is_floating_point<T>, decltype(arg * arg)>::type
{
    cout << "foo<T>(" << arg << ") optimized for floats" << endl;

    return arg * arg;
}

int main()
{
    foo(67);

    int x = 89;
    foo(x);

    short sx = 24;
    foo(sx);

    double dx = 5.444;
    foo(dx);

    float fx = 3.14F;
    cout << "fx^2 = " << foo(fx) << endl;
}

