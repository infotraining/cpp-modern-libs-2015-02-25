#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class Printer
{
public:
    void print(const string& msg, double arg)
    {
        cout << msg << arg << endl;
    }
};

class Logger
{
public:
    void log(const string& msg)
    {
        cout << "Log: " << msg << endl;
    }
};

void save_to_db(double temp)
{
    cout << "Saving to db: " << temp << endl;
}

class TemperatureMonitor
{
public:
    // zdefiniować typ sygnału TemperatureChangedSignal
    // TODO

    // zdefiniować typ slotów dla sygnału
    //TODO

    TemperatureMonitor() : temp_(0.0) {}

    // implementacja podłączenia slotu do sygnału
    // TODO

    void set_temp(double temp)
    {
        if (temp_ != temp)
        {
            temp_ = temp;
            // emisja sygnalu
            // TODO
        }
    }

private:
    // sygnał zmiany temperatury
    // TODO
    double temp_;
};
int main()
{
    using namespace boost::signals2;

    Printer printer;
    TemperatureMonitor monitor;
    Logger logger;

    // podłączenie Printera
    // TODO

    // podłączenie Loggera
    // TODO

    monitor.set_temp(24.0);
    monitor.set_temp(45.0);

    {
        // podłączenie save_to_db - ograniczone do zakresu
        // TODO

        monitor.set_temp(124.0);
        monitor.set_temp(245.0);
    }

    monitor.set_temp(45.0);

    {
        // podłączenie z shared_ptr<Printer>
        // TODO

        monitor.set_temp(400.0);
    }

    monitor.set_temp(45.0);
}
