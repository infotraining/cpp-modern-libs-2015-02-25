#include "Deleter.hpp"
#include "ToBeDeleted.hpp"

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
	real_test();
}
