#include <iostream>
#include <boost/ref.hpp>
#include <boost/utility.hpp>
#include "bitmap.hpp"

using namespace std;

int main()
{
    Bitmap bm(10);

    bm.draw();

    //Base* ptr_b = new Derived;

    cout << boost::addressof(bm) << endl;
}

