#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>
#include <boost/utility.hpp>
#include <type_traits>

using namespace std;

class AbsVisitor : public boost::static_visitor<double>
{
public:
    template <typename T>
    auto operator()(const T& x) const
        -> typename boost::disable_if<is_floating_point<T>, double>::type
    {
        return abs(x);
    }

    template <typename T>
    auto operator()(const T& x) const
        -> typename boost::enable_if<is_floating_point<T>, double>::type
    {
        return fabs(x);
    }
};

int main()
{
    typedef boost::variant<int, short, long, double, float, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie moduły liczb
    AbsVisitor abs_visitor;
    transform(vars.begin(), vars.end(), ostream_iterator<double>(cout, " "),
              boost::apply_visitor(abs_visitor));
}
