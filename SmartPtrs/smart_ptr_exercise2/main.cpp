#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <memory>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
    unsigned int devno_;
public:
	Device(unsigned int devno) : devno_{devno}
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Constructor of Device #" << devno << endl;
	}

    unsigned int device_no() const
    {
        return devno_;
    }

    virtual void run()
    {
        cout << "Device(id: " << devno_ << ")::run()" << endl;
    }

	virtual ~Device()
	{
		cout << "Destructor of Device #" << devno_ << endl;
	}
};

class SuperDevice : public Device
{
public:
    SuperDevice(unsigned int devno) : Device{devno}
    {
        cout << "Constructor of SuperDevice # " << devno << endl;
    }

    ~SuperDevice()
    {
        cout << "Destructor of SuperDevice #" << device_no() << endl;
    }

    void run() override
    {
        cout << "SuperDevice(id: " << device_no() << ")::run()" << endl;
    }
};

unique_ptr<Device> create_device(unsigned int devno)
{
    if (devno % 2 == 0)
        return unique_ptr<Device>(new Device(devno));
    else
        return unique_ptr<SuperDevice>(new SuperDevice(devno));
}

class Broker 
{
public:
    Broker(int devno1, int devno2) : dev1_(create_device(devno1)), dev2_(create_device(devno2))
	{
	}

    Broker(Broker&& source) = default;
    Broker& operator=(Broker&& source) = default;

    ~Broker()
    {
        cout << "~Broker()" << endl;
    }

    void start()
    {
        dev1_->run();
        dev2_->run();
    }

private:
    std::unique_ptr<Device> dev1_;
    std::unique_ptr<Device> dev2_;
};

class MoveTracer
{
public:
    MoveTracer() = default;

//    MoveTracer(const MoveTracer& mt)
//    {
//        cout << "MoveTracer(const MoveTracer&)" << endl;
//    }

//    MoveTracer& operator=(const MoveTracer& mt)
//    {
//        cout << "operator=(const MoveTracer&)" << endl;

//        return *this;
//    }

    MoveTracer(MoveTracer&& mt)
    {
        cout << "MoveTracer(MoveTracer&&)" << endl;
    }


    MoveTracer& operator=(MoveTracer&& mt)
    {
        cout << "operator=(MoveTracer&&)" << endl;

        return *this;
    }
};

int main()
{
//	try
//	{
//        Broker b(1, 3);
//        b.start();

//        Broker cb = move(b);
//        cb.start();
//	}
//	catch(const exception& e)
//	{
//		cerr << "Wyjatek: " << e.what() << endl;
//	}

    const MoveTracer cmt;

    MoveTracer m1 = move(cmt);
}
