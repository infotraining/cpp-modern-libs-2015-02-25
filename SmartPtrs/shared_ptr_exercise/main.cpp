#include <iostream>
#include <set>
#include <boost/lexical_cast.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>
#include <memory>
#include <boost/any.hpp>

class Observer
{
public:
    virtual void update(boost::any args) = 0;
    virtual ~Observer() {}
};

class Subject
{
    int state_;
    std::set<std::weak_ptr<Observer>, std::owner_less<std::weak_ptr<Observer>>> observers_;

public:
    Subject() : state_(0)
    {}

    void register_observer(std::weak_ptr<Observer> observer)
    {
        observers_.insert(observer);
    }

    void unregister_observer(std::weak_ptr<Observer> observer)
    {
        observers_.erase(observer);
    }

    void set_state(int new_state)
    {
        if (state_ != new_state)
        {
            state_ = new_state;
            notify("Changed state on: " + boost::lexical_cast<std::string>(state_));
        }
    }

protected:
    void notify(const std::string& event_args)
    {
        auto it = observers_.begin();

        while (it != observers_.end())
        {
            auto observer = it->lock();

            if (observer)
            {
                observer->update(boost::any(event_args));
                ++it;
            }
            else
                observers_.erase(it++);
        }
    }
};

class ConcreteObserver1 : public std::enable_shared_from_this<ConcreteObserver1>, public Observer
{
public:
    virtual void update(boost::any event)
    {
        std::cout << "ConcreteObserver1: " << boost::any_cast<std::string>(event) << std::endl;
    }

    void register_me(Subject& s)
    {
        s.register_observer(shared_from_this());
    }
};

class ConcreteObserver2 : public Observer
{
public:
    virtual void update(boost::any event)
    {
        std::cout << "ConcreteObserver2: " << boost::any_cast<std::string>(event) << std::endl;
    }
};

int main(int argc, char const *argv[])
{
    using namespace std;

    Subject s;

    auto o1 = make_shared<ConcreteObserver1>();
    o1->register_me(s);

    {
        auto o2 = make_shared<ConcreteObserver2>();
        s.register_observer(o2);

        s.set_state(1);

        cout << "End of scope." << endl;
    }

    s.set_state(2);
}
