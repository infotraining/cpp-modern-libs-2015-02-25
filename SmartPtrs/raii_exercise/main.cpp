#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <memory>
#include <vector>
#include <boost/noncopyable.hpp>

const char* get_line()
{
	static unsigned int count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if (!file)
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard : boost::noncopyable
{
    FILE* file_;

public:
    explicit FileGuard(FILE* file) : file_{file}
    {
    }

    ~FileGuard()
    {
        if (file_)
            fclose(file_);
    }

    explicit operator bool() const
    {
        return file_;
    }

    FILE* get() const
    {
        return file_;
    }
};

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file{fopen(file_name, "w")};

    // FileGuard file_copy = file;
    // FileGuard file_moved = std::move(file);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

//unsigned int gen_id()
//{
//   static unsigned int id = 0;

//   return ++id;
//}

//struct NonAggregate
//{
//    int x;
//    double dx;

//    NonAggregate(double dx) : x{gen_id()}, dx(dx) {}
//};

//NonAggregate agg1 {2.4};

//std::vector<int> vec = { 1, 2, 3, 4, 5 };
//std::vector<int> v1(3, 5);
//std::vector<int> v3{3, 5.6};

void save_to_file_with_up(const char* file_name)
{
    auto file_closer = [](FILE* f) { fclose(f); };

    std::unique_ptr<FILE, decltype(file_closer)> file(fopen(file_name, "w"), file_closer);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_sp(const char* file_name)
{
    std::shared_ptr<FILE> file(fopen(file_name, "w"),
                               [](FILE* f) { if (f) fclose(f); });

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    save_to_file_with_raii("text.txt");

    std::cout << "End of main..." << std::endl;
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

	std::cout << "Press a key..." << std::endl;
    std::string temp;
    std::getline(std::cin, temp);
}
