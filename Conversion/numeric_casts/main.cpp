#include <iostream>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

int main()
{
    int x = 6324;

    short sx = boost::numeric_cast<short>(x);

    cout << "x: " << x << "; sx: " << sx << endl;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << "x: " << x << "; ux: " << ux << endl;

    string str =  "ux = " + boost::lexical_cast<string>(ux);

    cout << str << endl;

    string number = "656";

    cout << (boost::lexical_cast<int>(number) + 1) << endl;

    str += "pi = " + to_string(3.14);
}

