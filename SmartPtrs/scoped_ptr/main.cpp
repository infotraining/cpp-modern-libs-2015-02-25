#include <iostream>
#include <stdexcept>
#include <memory>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <cstdlib>

using namespace std;

class Gadget
{
    static unsigned int counter_;
public:
    Gadget() : id_(++counter_)
    {
        cout << "Konstruktor Gadget(" << id_ << ")" << endl;
    }

    Gadget(int id) : id_{id}
    {}

    virtual ~Gadget()
    {
        cout << "Destruktor ~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void do_stuff() const
    {
        cout << "Gadget::do_stuff()" << endl;
    }

private:
    int id_;
};

unsigned int Gadget::counter_ = 0;

class User
{
    std::auto_ptr<Gadget> gadget_;
public:
    User() : gadget_{new Gadget}
    {
    }

    void use_gadget()
    {
        gadget_->do_stuff();
    }
};

auto_ptr<Gadget> create_gadget(int arg)
{
    return auto_ptr<Gadget>(new Gadget(arg));
}

void sink(auto_ptr<Gadget> arg)
{
    cout << "sink..." << endl;
    arg->do_stuff();
}


int main()
{
	try
	{
		boost::scoped_ptr<Gadget> my_gadget(new Gadget());

        (*my_gadget).do_stuff();
		my_gadget->do_stuff();
        my_gadget.reset(new Gadget());

		boost::scoped_ptr<Gadget> your_gadget(new Gadget());
		
		your_gadget->do_stuff();
		
		throw std::runtime_error("Błąd");
		
		your_gadget->do_stuff();

        User user;
        user.use_gadget();

        User copy_of_user = user;
        user.use_gadget(); // UB
	}
	catch (...)
	{
		cout << "Obsługa wyjątku." << endl;
	}

	cout << "--------------------------------------\n";

	try
	{
		// prezenatacja dzialania scoped_array
		boost::scoped_array<Gadget> gadgets(new Gadget[10]);

		for(size_t i = 0; i < 10; ++i)
			gadgets[i].do_stuff();

		throw std::runtime_error("Runtime Exception");
	}
	catch (...)
	{
		cout << "Obsluga wyjatku" << endl;
	}

    cout << "--------------------------------------\n";

    {
        auto_ptr<Gadget> g = create_gadget(113);

        g->do_stuff();

        sink(create_gadget(888));

        sink(g);

        cout << "after sink..." << endl;

        //g->do_stuff();  // UB

        auto_ptr<Gadget> g2 = create_gadget(999);

        Gadget* raw_ptr = g2.release();

        delete raw_ptr;
    }
}

