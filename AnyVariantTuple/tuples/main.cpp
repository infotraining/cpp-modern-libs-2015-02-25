#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>
#include <numeric>
#include <functional>

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    decltype(data.begin()) min_it;
    decltype(data.begin()) max_it;

    tie(min_it, max_it) = minmax_element(data.begin(), data.end());

    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_it, *max_it, avg);
}

class Person
{
    string fname_;
    string lname_;
    int age_;
public:
    Person(string fn, string ln, int age) : fname_{fn}, lname_{ln}, age_{age}
    {}

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }

    bool operator<(const Person& p) const
    {
        return tied() < p.tied();
    }
private:
    tuple<const string&, const string&, const int&> tied() const
    {
        return tie(lname_, fname_, age_);
    }
};

int main()
{
    vector<int> data = { 135, 534, 234, 21, 534, 754, 568, 23, 232, 678, 9 };

    tuple<int, int, double> results = calc_stats(data);

    cout << "min: " << get<0>(results)
         << " max: " << get<1>(results)
         << " max: " << get<2>(results) << endl;

    int a {10};
    string b = "text";
    double c {3.14};

    tuple<int&, const string&, double&> ref_tuple {a, b, c};

    get<0>(ref_tuple) += 10;
    //get<1>(ref_tuple) += "text";
    get<2>(ref_tuple)++;

    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << "c: " << c << endl;

    tuple<int&, const string&> ref_tuple_2 = make_tuple(ref(a), cref(b));

    get<0>(ref_tuple_2) += 10;

    cout << "a: " << a << endl;
    cout << "b: " << b << endl;

    int min, max;
    double avg;

    //tuple<int&, int&, double&> results_tied{min, max, avg};
    //results_tied = calc_stats(data);

    tie(min, ignore, avg) = calc_stats(data);

    cout << "min: " << min
         //<< " max: " << max
         << " avg: " << avg << endl;
}

