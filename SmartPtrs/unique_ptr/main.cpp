#include <iostream>
#include <stdexcept>
#include <memory>
#include <vector>

using namespace std;

class Gadget
{
    static unsigned int counter_;
public:
    Gadget() : id_(++counter_)
    {
        cout << "Konstruktor Gadget(" << id_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "Destruktor ~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void do_stuff() const
    {
        cout << "Gadget::do_stuff()" << endl;
    }

private:
    int id_;
};

class SuperGadget : public Gadget
{
public:
    SuperGadget()
    {
        cout << "Konstrktor SuperGadget(" << id() << ")" << endl;
    }

    ~SuperGadget()
    {
        cout << "Destruktor ~SuperGadget( " << id() << ")" << endl;
    }

    void do_stuff() const override
    {
        cout << "SuperGadget::do_stuff()" << endl;
    }

	void special_stuff()
	{
		cout << "SuperGadget(id: " << id() << ")::special_stuff()" << endl;
	}
};

enum class GadgetType
{
    normal, super
};

std::unique_ptr<Gadget> create_gadget(GadgetType type)
{
    std::unique_ptr<Gadget> gadget; // intialized with nullptr

    if (type == GadgetType::normal)
        gadget.reset(new Gadget());
    else if (type == GadgetType::super)
        gadget.reset(new SuperGadget());
    else
        throw std::runtime_error("Bad gadget type");

    return gadget;
}

unsigned int Gadget::counter_ = 0;

void sink(unique_ptr<Gadget> g)
{
    cout << "sink..." << endl;
    g->do_stuff();
}

void container_of_up(unique_ptr<Gadget> your_gadget)
{
    {
        vector<unique_ptr<Gadget>> gadgets;

        // vector<T>.push_back(const T&);
        // vector<T>.push_back(T&&);
        gadgets.push_back(unique_ptr<Gadget>(new Gadget()));
        gadgets.push_back(create_gadget(GadgetType::super));

        gadgets.push_back(move(your_gadget));

        for(const auto& g : gadgets)
            g->do_stuff();

        gadgets.clear();
    }

    cout << "after scope..." << endl;
}

int main()
{
    // block of code
    {
        std::unique_ptr<Gadget> my_gadget{ new Gadget() };

        my_gadget->do_stuff();
    }

    cout << "\n\n";

    auto my_gadget = create_gadget(GadgetType::super);

    my_gadget->do_stuff();

    /*
     * template <T>
     * class unique_ptr
     * {
     * public:
     *   unique_ptr(const unique_ptr&) = delete;
     *   unique_ptr(unique_ptr&& source); // move constructor
     * };
     */

    std::unique_ptr<Gadget> your_gadget = move(my_gadget);

    your_gadget->do_stuff();

    sink(move(your_gadget));

    your_gadget.reset(new Gadget());

	SuperGadget* ptr_super = static_cast<SuperGadget*>(your_gadget.get());

	ptr_super->special_stuff();

    cout << "\n\n-----------------------\n";

    {
        shared_ptr<Gadget> sp_gadget = create_gadget(GadgetType::super);

        sp_gadget->do_stuff();

        shared_ptr<SuperGadget> sp_super_gadget = static_pointer_cast<SuperGadget>(sp_gadget);

        sp_super_gadget->special_stuff();
    }

    cout << "\n\n-----------------------\n";

    shared_ptr<Gadget> ptr_g(new SuperGadget);

    ptr_g->do_stuff();
}

