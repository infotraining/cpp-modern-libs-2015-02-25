#include <iostream>
#include <string>
#include <tuple>

using namespace std;

class Base
{
public:
	virtual ~Base() = default;

	virtual string test() const
	{
		return "Base::test()";
	}
};

class Derived : public Base
{
public:
	virtual string test() const override
	{
		return "Derived::test()";
	}
};

int main()
{
	tuple<int, const char*, Derived> t1(-5, "Krotka1", Derived());
	tuple<unsigned int, string, Base> t2;

	t2 = t1;

	cout << "get<0>(t2) = " << get<0>(t2) << "\n"
		 << "get<1>(t2) = " << get<1>(t2) << "\n"
		 << "get<2>(t2).test() = " << get<2>(t2).test() << "\n";
}
