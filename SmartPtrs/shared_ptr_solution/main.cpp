#include <iostream>
#include <set>
#include <memory>
#include <boost/lexical_cast.hpp>

class Observer
{
public:
    virtual void update(const std::string& event_args) = 0;

    virtual ~Observer() {}
};

class Subject
{
    int state_;
    std::set<std::weak_ptr<Observer>, std::owner_less<std::weak_ptr<Observer>>> observers_;

public:
    Subject() : state_(0) {}

    void register_observer(std::weak_ptr<Observer> observer)
    {
        observers_.insert(observer);
    }

    void unregister_observer(std::weak_ptr<Observer> observer)
    {
        observers_.erase(observer);
    }

    void set_state(int new_state)
    {
        if (state_ != new_state)
        {
            state_ = new_state;
            notify("Changed state on: " + boost::lexical_cast<std::string>(state_));
        }
    }

protected:
    void notify(const std::string& event_args)
    {
        std::set<std::weak_ptr<Observer>>::iterator it = observers_.begin();

        while (it != observers_.end())
        {
            if (std::shared_ptr<Observer> observer_ptr = it->lock())
            {
                observer_ptr->update(event_args);
                ++it;
            }
            else
            {
                std::cout << "Dangling pointer found. Removal from observers list." << std::endl;
                it = observers_.erase(it);
            }
        }
    }
};

class ConcreteObserver1 : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver1: " << event << std::endl;
    }
};

class ConcreteObserver2 : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver2: " << event << std::endl;
    }
};

class ConcreteObserver3 : public Observer, public std::enable_shared_from_this<ConcreteObserver3>
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver3: " << event << std::endl;
    }

    void register_me(Subject& s)
    {
        s.register_observer(shared_from_this());
    }
};

int main(int argc, char const* argv[])
{
    using namespace std;

    Subject s;

    auto o1 = make_shared<ConcreteObserver1>();
    s.register_observer(o1);

    auto o3 = make_shared<ConcreteObserver3>();
    {
        auto o2 = make_shared<ConcreteObserver2>();
        s.register_observer(o2);

        o3->register_me(s);

        s.set_state(1);

        cout << "End of scope." << endl;
    }

    s.set_state(2);
}
