#include <iostream>
#include <boost/utility.hpp>

class CodeBreaker
{
public:
	int operator&() const
	{
		return 13;
	}
};

template <typename T>
void print_address(const T& t)
{
	std::cout << "Adres: " << (&t) << std::endl;
}

template <typename T>
void smart_print_address(const T& t)
{
	std::cout << "Adres: " << boost::addressof(t) << std::endl;
}


int main()
{
	CodeBreaker cb;

	print_address(cb);
	smart_print_address(cb);
}
